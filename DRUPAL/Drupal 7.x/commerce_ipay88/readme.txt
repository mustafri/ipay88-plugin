/*
	@author: ChiPV
	Company: ipay88
	Ubercart Plugin: Only use for Indonesia
*/

==============
Installation
==============

Upload folder "commerce_ipay88" to: path_root_drupal/sites/all/modules

- Login to Drupal CMS with Admin account
- Go to Site Settings >> Modules 
- Then Enable Module: iPay88 (commerce_ipay88)
- Go to Store Settings >> Payment Methods >> Enable "iPay88 Payment Gateway Solution"
- After enabled, click "Edit" link.
- Under Actions Section, click "edit" button under the operations.
- Under the "Payment settings", enter the merchant code and merchant key provided by iPay88.
- Enable the bank that you want customer pay into.