/**
 * Created by shinichi on 2/3/16.
 */
window.onload = function() {
    if (typeof jQuery != 'undefined') {
        // jQuery is loaded => print the version, use for debugging
        //console.log(jQuery.fn.jquery);
    }
    if (window.jQuery) {
        jQuery(document).ready(function($) {
            var jQueryVersion = jQuery.fn.jquery;

            if(jQueryVersion >= 1.7) {
                $("input[name='commerce_payment[payment_details][ipay88_payment_methods]").on('click', function() {
                    var paymentId = $(this).val();

                    console.log(paymentId);
                    $("input[name='commerce_payment[payment_details][ipay88_payment_method_selected]").val(paymentId);
                })
            }
            else {
                $("input[name='commerce_payment[payment_details][ipay88_payment_methods]").live('click', function() {
                    var paymentId = $(this).val();

                    console.log(paymentId);
                    $("input[name='commerce_payment[payment_details][ipay88_payment_method_selected]").val(paymentId);
                })
            }
        });
    }
}
