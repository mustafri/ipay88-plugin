=== WP Charitable ipay88-charitable v1.0.1 ===

1. Requires at least WP Charitable: 1.6.4
2. This plugin allows you to use iPay88 Payment Gateway within your WP Charitable.


=== Important Note ===

The plugin only support one (1) currency during checkout and it do NOT handle any currency conversion.


== Installation ==

1. Upload the ipay88-charitable v1.0.1.zip to the 'Plugins > Add New menu' in WordPress.
2. Activate the plugin through the 'Plugins' menu.
3. Navigate to 'CHaritable > Settings > Payment Gateways' to configure the iPay88 Payment Gateway settings.


Steps for configuration of your iPay88 installation.
	1. Enable Gateway iPay88 Payment Gateway.  
	2. Set your "Gateway Label". These options are seen on the donation form.
	3. Enter your "Merchant Code".
	4. Enter your "Merchant Key".
	5. Check Sandbox for testing purpose only.	 
	7. Save Changes.


== Options ==

1. Gateway Label: The label that will be shown to donors on the donation form.
2. Merchant Code: The Merchant Code provided by iPay88 and used to uniquely identify the merchant.
3. Merchant Key: Provided by iPay88 and shared between iPay88 and merchant only.
4. Sandbox: Sandbox mode provides you with a chance to test your gateway integration with iPay88. The payment requests will be send to the iPay88 sandbox URL. Disable to start accepting Live payments.











