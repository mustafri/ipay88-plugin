<?php
/**
 * iPay88 Gateway class
 *
 * @version     1.0.0
 * @package     Charitable/Classes/Charitable_Gateway_ipay88
 * @author      alda.mustafri@ipay88.co.id
 */

// Exit if accessed directly 
if (! defined('ABSPATH')) 
{
    exit();
} 

if (! class_exists('Charitable_Gateway_ipay88')) :

    // iPay88 Gateway
  	class Charitable_Gateway_ipay88 extends Charitable_Gateway
    {
        // @var string
        const ID = 'ipay88';

        // Instantiate the gateway class, defining its key values.
        // @access public
        // @since 1.0.0
       
        public function __construct()
        {
            $this->name	= apply_filters('charitable_gateway_ipay88_name', __('iPay88 Payment Gateway', 'charitable-ipay88'));
            $this->defaults = array('label' => __('iPay88 Payment Gateway', 'charitable-ipay88'));
            $this->supports = array('1.3.0');
            
            //Needed for backwards compatibility with Charitable < 1.3
            $this->credit_card_form = false;
		}

        // returns the current gateway's ID.
        // @return string
        // @access public
        // @static
        // @since 1.0.3
		
        public static function get_gateway_id()
        {
            return self::ID;
        }
       
        // register gateway settings.
      	// @param array $settings
        // @return array
        // @access public
        // @since 1.0.0
       
	    public function gateway_settings($settings)
        {		
			// Notice in IDR only 
            if ('IDR' != charitable_get_option('currency', 'IDR')) 
			{
                $settings['currency_notice'] = array(
                    'type' 			=> 'notice',
                    'content' 		=> $this->get_currency_notice(),
                    'priority' 		=> 1,
                    'notice_type' 	=> 'error'
                );
            }
            
			// Notice default payment gateway
            if ('ipay88' != charitable_get_option('default_gateway')) 
			{
            	$settings['default_gateway_notice'] = array(
                	'type' 			=> 'notice',
                    'content' 		=> $this->get_default_gateway_notice(),
                    'priority' 		=> 2,
                    'notice_type' 	=> 'error'
                );
            }
				
            $settings['merchant_code'] = array(
                'type' 		=> 'text',
                'title' 	=> __('Merchant Code', 'charitable-ipay88'),
                'priority' 	=> 6,
                'help' 		=> 'The Merchant Code provided by iPay88 and used to uniquely identify the merchant.',
                'required' 	=> true,
            );

            $settings['merchant_key'] = array(
                'type' 		=> 'text',
                'title'		=> __('Merchant Key', 'charitable-ipay88'),
                'priority'	=> 8,
                'help' 		=> 'Provided by iPay88 and shared between iPay88 and merchant only.',
                'required' 	=> true,
            );

        	$settings['sandbox'] = array(
                'type' 		=> 'checkbox',
                'title' 	=> __('Sandbox', 'charitable-ipay88'),
                'priority' 	=> 10,
				'help' 		=> 'Sandbox mode provides you with a chance to test your gateway integration with iPay88. The payment requests will be send to the iPay88 sandbox URL.<br/>Disable to start accepting Live payments.',
                'default'	=> 'yes'

            );
			
        	return $settings;
        }
	
		
		// cancel url
        private function getCancelURL($donation)
        {
            $cancel_url = charitable_get_permalink('donation_cancel_page', array('donation_id' => $donation->ID));
            
            if (! $cancel_url) 
			{
            	$cancel_url = esc_url(add_query_arg(
				array('donation_id' => $donation->ID, 'cancel' => true), wp_get_referer()));
            }
            
			return $cancel_url;
        }
		
       
	    // Process the donation with ipay88.
        // @param Charitable_Donation $donation
        // @return void
        // @access public
        // @static
        // @since 1.0.0
        public static function process_donation($content, Charitable_Donation $donation)
        {
		    $gateway = new Charitable_Gateway_ipay88();
         			 
			//notice rupiah
			if ("IDR" != charitable_get_option('currency')) 
			{
                $error_notice = "iPay88 only accepts payments in Indonesia Rupiah.";
				
				echo ("<div class='charitable-notice charitable-form-errors'>Error:");
               	echo ("<div>".$error_notice."</div>");
				echo ("<a href=".$gateway->getCancelURL($donation).">Go Back</a></div>");			
            } 
			
			else 
			{				
				$donor 			= $donation->get_donor();
				
            	$first_name 	= $donor->get_donor_meta('first_name');
           		$last_name 		= $donor->get_donor_meta('last_name');
				$username		= $first_name.' '.$last_name;
            	$useremail 		= $donor->get_donor_meta('email');
                $usercontact 	= $donor->get_donor_meta('phone');
                       
				$donation_key	= $donation->get_donation_key();
               	$product_info 	= html_entity_decode($donation->get_campaigns_donated_to(), ENT_COMPAT, 'UTF-8' );	
        	    
				$payment_gateway =  $donation->get_gateway();
				
				//Date
				date_default_timezone_set('Asia/Jakarta');	
				
				$today 				= date("m/d/Y");
				$now				= date("h a");	
				$ori_date 			= $donation->get_date();
				$get_date			= date("m/d/Y", strtotime($ori_date));
				$ori_time 			= $donation->get_time();
				$get_time			= date("h a", strtotime($ori_time));
				$get_status 		= $donation->get_status();
				$get_curent_user	= $donation->is_from_current_user();
							
			
				if( $get_status == "charitable-pending" && $get_curent_user == "1"  && $payment_gateway == "ipay88" &&
				 	$get_date == $today && $get_time == $now)
				{		
					// Admin gateway settings		
					$merchant_code 	= $gateway->get_value('merchant_code');		
					$merchant_key 	= $gateway->get_value('merchant_key');			   	
					$sandbox		= $gateway->get_value('sandbox');		
					$image_path 	= plugins_url('assets/images', __FILE__ );
					$css 			= plugins_url('assets/css/ipay88.css', __FILE__ );
								
					// Amount
					$total 			= $donation->get_total_donation_amount(true);
					$orderAmount	= number_format($total, 2, ".", "");
					$HashAmount 	= str_replace(".","",str_replace(",","",$orderAmount));	
					
					// Currency
					$currency_iso_code 	= charitable_get_option('currency');
						
					// Signature
					$MechantKey 	= $merchant_key;
					$MerchantCode 	= $merchant_code;
					$RefNo			= $donation->ID; 
					$Amount			= $HashAmount; 
					$Currency		= $currency_iso_code;
					$ipaySignature 	= "";
					
					$encrypt		= sha1($MechantKey.$MerchantCode.$RefNo.$Amount.$Currency);		
					
					for ($i=0; $i<strlen($encrypt); $i=$i+2)
					{
						$ipaySignature .= chr(hexdec(substr($encrypt,$i,2)));
					}
					
					$ipaySignature = base64_encode($ipaySignature);
					
					// Action					
					if($sandbox == "1")
					{
						$action = "https://sandbox.ipay88.co.id/epayment/entry.asp";
					}
					
					else
					{
						$action = "https://payment.ipay88.co.id/epayment/entry.asp";
					}
				
					// Response URL
					$response_url	= get_option( 'siteurl' ).'/donation-receipt/'.$RefNo.'';
					$return_url 	= charitable_get_permalink('donation_receipt_page', 
									  array('donation_id' => $donation->ID));				
									  		
					//Backend URL
					$plugins_url 	= plugins_url();	
					$backend_url 	= $plugins_url.'/ipay88-charitable/ipay88_backend.php';
					
					//$backend_url	= get_option( 'siteurl' ).'/ipay88_backend';
					//$backend_url	= plugins_url('ipay88_backend.php', __FILE__ );
																		
					$ipay88_request = array (
										'MerchantCode'	=> $MerchantCode,
										'PaymentId'		=> "",
										'RefNo' 		=> $RefNo,
										'Amount' 		=> $Amount,
										'Currency'		=> $currency_iso_code,
										'ProdDesc' 		=> $product_info,
										'UserName' 		=> $username,
										'UserEmail' 	=> $useremail,
										'UserContact' 	=> $usercontact,
										'Remark' 		=> "Transaction date ".date('Y-m-d H:i:s'),
										'Lang'			=> "UTF-8",									
										'Signature'		=> $ipaySignature,
										'ResponseURL' 	=> $response_url,
										'BackendURL'  	=> $backend_url,
									);
			
					$key = array_keys($ipay88_request);
																	
					echo "<link rel='stylesheet' href='".$css."'>";
					echo "<center><h2>Please wait while redirecting...</h2></center>";	
					//echo "<center><img src='".$image_path."/ipay88-loader.gif'></center>";		
					echo "<script src='//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js'></script>";
					
					$load_file_function = '
					<script language="JavaScript">
					$(document).ready(function() 
					{
						document.ipay88_request_paramaters.submit();
					});
					</script>';
		
					echo $load_file_function;					
					
					echo "<body>";
					echo "<form  name='ipay88_request_paramaters' action='".$action."' method='POST'>";

					for($i=0;$i<count($key);$i++) 
					{
						echo "<input type='hidden' name='".$key[$i]."' value='".$ipay88_request[$key[$i]]."' />";
					}
					
					echo "</form>";
				}
						
				// disable for type url on browser
				else
				{
					wp_redirect(get_option('siteurl'));
					exit;
				}	
            }
			 
			$content = ob_get_clean();
            return $content;
        }


      	// update the donation's log.
        public static function update_donation_log($donation, $message)
        {
            if (version_compare(charitable()->get_version(), '1.4.0', '<')) 
			{
                return Charitable_Donation::update_donation_log($donation->ID, $message);
            }
            
            return $donation->update_donation_log($message);
        }

        
        // set the phone field to be required in the donation form.
        public static function set_phone_field_required($fields)
        {
            $fields['phone']['required'] = true;
            return $fields;
        }

        
       	// currency notice
        public function get_currency_notice()
        {
            ob_start();
                        
            printf(__('iPay88 only accepts payments in Indonesia Rupiah. %sChange Now%s', 'charitable-ipay88'),
                '<a href="#" class="button" data-change-currency-to-idr>', '</a>')?>
			
			<script>
			(function($)
			{
				$('[data-change-currency-to-idr]').on('click', function()
				{		
					var $this = $(this);

					$.ajax(
					{
						type	: "POST",
						data 	: {
									action: 'charitable_change_currency_to_idr', 
									_nonce: "<?php echo wp_create_nonce( 'ipay88_currency_change' ) ?>"
							  	  },
						url		: ajaxurl,
						success	: function (response) 
								  {
									console.log( response );
									if ( response.success ) 
									{
										$this.parents( '.notice' ).first().slideUp();
									}            
								   }, 
						error	: function( response ) 
								  {
									console.log( response );
								  }
					});
				})
			})( jQuery );
			</script>
			
			<?php
           return ob_get_clean();
        }

        
		// change the currency to idr
        public static function change_currency_to_idr()
        {
            if (! wp_verify_nonce($_REQUEST['_nonce'], 'ipay88_currency_change')) 
			{
                wp_send_json_error();
            }
            
            $settings = get_option('charitable_settings');
            $settings['currency'] = 'IDR';
            $updated = update_option('charitable_settings', $settings);
            
            wp_send_json(array('success' => $updated));
            wp_die();
        }

      
      	// gateway notice.
        public function get_default_gateway_notice()
        {
            ob_start();
                  
            printf(__('iPay88 is not set as default payment gateway. %sSet as Default%s', 'charitable-ipay88'),
                '<a href="#" class="button" data-change-default-gateway>', '</a>')?>

			<script>
			(function($)
			{
				$('[data-change-default-gateway]').on('click', function() 
				{
					var $this = $(this);
			
					$.ajax({
								type: "POST",
								data: {
								action: 'charitable_change_gateway_to_ipay88', 
								_nonce: "<?php echo wp_create_nonce( 'ipay88_gateway_change' ) ?>"
							},
						
					url: ajaxurl,
					success: function(response) 
					{
						console.log(response);
						if (response.success)
						{
							$this.parents('.notice').first().slideUp();
						}            
					}, 
					error: function(response) 
					{
						console.log(response);
					}
				});
				})
			})( jQuery );
			</script>
			
			<?php
            return ob_get_clean();
        }


        // change the default gateway to ipay88
        public static function change_gateway_to_ipay88()
        {			
            if (!wp_verify_nonce($_REQUEST['_nonce'], 'ipay88_gateway_change')) 
			{
                wp_send_json_error();
            }
            
            $settings = get_option('charitable_settings');
            $settings['default_gateway'] = "ipay88";
            $updated = update_option('charitable_settings', $settings);
            
            wp_send_json(array('success' => $updated));
            wp_die();
        }
		
		
		// Redirect the donation to the processing page.
        // @param int $donation_id
        // @return void
        // @access public
        // @static
        // @since 1.0.0
      
        public static function redirect_to_processing_legacy($donation_id)
        {
            wp_safe_redirect(charitable_get_permalink('donation_processing_page',
            array('donation_id' => $donation_id)));
            
            exit();
        }
				
		
		// response url		
		public static function ipay88_response(Charitable_Donation $donation)
        {
			$gateway 		= new Charitable_Gateway_ipay88();	
			$gateway_name 	= $donation->get_gateway();
			
			$donor 				= $donation->get_donor();
			$donor_first_name 	= $donor->get_donor_meta('first_name');
           	$donor_last_name 	= $donor->get_donor_meta('last_name');
			$donor_username		= $donor_first_name.' '.$donor_last_name;
            $donor_useremail 	= $donor->get_donor_meta('email');
           	
			$donation_product 	= html_entity_decode($donation->get_campaigns_donated_to(), ENT_COMPAT, 'UTF-8' );	
			$donation_total		= $donation->get_total_donation_amount(true);
		
			$admin_email 		= get_option('admin_email');		
			$site_info			= get_bloginfo($show, $filter);
					
					
			if($gateway_name == "ipay88")
			{
				// go with ipay88!
				if (isset($_REQUEST['TransId']) && isset($_REQUEST['Signature'])) 
				{
					//Get signature response
					$signature_response		= $_REQUEST['Signature'];
				
					//Generate signature	
					$MechantKey_response 	= $gateway->get_value('merchant_key');			  
					$MerchantCode_response 	= $gateway->get_value('merchant_code');		
					$PaymentId_response		= $_REQUEST['PaymentId'];
					$RefNo_response 		= trim(stripslashes($_REQUEST['RefNo']));
					$HashAmount_response 	= str_replace(array(',','.'), "", $_REQUEST['Amount']);
					$Currency_response 		= $_REQUEST['Currency'];
					$Status_response 		= $_REQUEST['Status'];
					$error_response         = $_REQUEST['ErrDesc'];
					$transid_response       = $_REQUEST['TransId'];
				
					$merchant_signature		= "";
					
					$merchant_encrypt		= sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$HashAmount_response.$Currency_response.$Status_response);		
			
					for ($i=0; $i<strlen($merchant_encrypt); $i=$i+2)
					{	
						$merchant_signature .= chr(hexdec(substr($merchant_encrypt,$i,2)));
					}
			
					$merchant_signature_check = base64_encode($merchant_signature);
				
					//Payment success and signature match
					if ($_REQUEST['Status']=="1" && $merchant_signature_check==$signature_response) 
					{			
						$message_admin = "iPay88 Payment Completed. Transaction ID: ".$transid_response.".";
						self::update_donation_log($donation, $message_admin);
						
						//update from ipay88_backend
						//$donation->update_status('charitable-completed');
						//die( __( 'Donation Completed', 'charitable' ) );
					    //$donation->send_with_donation_id( $RefNo_response ); 
						
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						
						$subject = "Thank you for your donation";
						$headers = 'From: '. $admin_email . "\r\n" .
    							   'Reply-To: ' . $admin_email . "\r\n";
						
						$body = '
						<html>	
						<body>
					
							<div id="email_container" style="background:#FFFFFF">          
							<div style="width:570px; padding:0 0 0 20px; margin:50px auto 12px auto" id="email_header">
								<span style="background:#C9C9C9; color:#fff; padding:12px;font-family:trebuchet ms; 
								letter-spacing:1px; -moz-border-radius-topleft:5px; -webkit-border-top-left-radius:5px; 
								border-top-left-radius:5px;moz-border-radius-topright:5px; 
								-webkit-border-top-right-radius:5px; border-top-right-radius:5px;">'.$site_info.'</span>
							</div>
							</div>
					
							<div style="width:550px; padding:0 20px 20px 20px; background:#fff; margin:0 auto; 
							border:1px #BBBBBB solid; moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; color:#454545;line-height:1.5em; " id="email_content">						
						
							<h1 style="padding:5px 0 0 0; font-family:georgia;font-weight:500; font-size:24px;color:#000;">
							Your Donation Receipt</h1>
						
								<p>Dear '.$donor_username.',</p>
								<p>Thank you so much for your generous donation.</p>
								
								<p>
								<b>Your Donation Details</b><br>
								'.$donation_product.': IDR '.$donation_total.'
								</p>
							
								<p>With thanks, '.$site_info.'</p>
								
								<div style="text-align:center; border-top:1px solid #eee;
								padding:5px 0 0 0;" id="email_footer"> 
								<small style="font-size:11px; color:#999; line-height:14px;">
								</small>
								</div>
					
							</div>
						 
						</body>
						</html>';
						
						//Here put your Validation and send mail
						$sent = wp_mail($donor_useremail, $subject, $body, $headers);
						
						if($sent) 
						{
							$message_admin = "Email already sent to donor.";
							self::update_donation_log($donation, $message_admin);
						}

						else  
						{
							$message_admin = "Email wasn't sent to donor.";
							self::update_donation_log($donation, $message_admin);
						}
					
						return;			
					}
					
					//Payment pending and signature match
					elseif ($_REQUEST['Status']=="6" && $merchant_signature_check==$signature_response)
					{			
						$message_admin = "iPay88 Payment Pending. Transaction ID: ".$transid_response.".";
						self::update_donation_log($donation, $message_admin);		
						
						$donation->update_status('charitable-pending');
						
						$message_customer = "<center><h3>Your transaction is pending. Please make a payment.</h3></center>
						<br><br><span style='float:right'><a href=".$gateway->getCancelURL($donation).">Go Back</a></span>";
						
						
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						
						$subject = "Thank you for your donation";
						$headers = 'From: '. $admin_email . "\r\n" .
    							   'Reply-To: ' . $admin_email . "\r\n";
						
						$body = '
						<html>	
						<body>
					
							<div id="email_container" style="background:#FFFFFF">          
							<div style="width:570px; padding:0 0 0 20px; margin:50px auto 12px auto" id="email_header">
								<span style="background:#C9C9C9; color:#fff; padding:12px;font-family:trebuchet ms; 
								letter-spacing:1px; -moz-border-radius-topleft:5px; -webkit-border-top-left-radius:5px; 
								border-top-left-radius:5px;moz-border-radius-topright:5px; 
								-webkit-border-top-right-radius:5px; border-top-right-radius:5px;">'.$site_info.'</span>
							</div>
							</div>
					
							<div style="width:550px; padding:0 20px 20px 20px; background:#fff; margin:0 auto; 
							border:1px #BBBBBB solid; moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; color:#454545;line-height:1.5em; " id="email_content">						
						
							<h1 style="padding:5px 0 0 0; font-family:georgia;font-weight:500; font-size:24px;color:#000;">
							Your Donation Receipt</h1>
						
								<p>Dear '.$donor_username.',</p>
								<p>Thank you so much for your generous donation.</p>
								
								<p>
								<b>Your Donation Details</b><br>
								'.$donation_product.': IDR '.$donation_total.'
								</p>
								
                                <p>Your transaction is pending.<b>Please complete your payment.</b></p>
                                
								<p>With thanks, '.$site_info.'</p>
								
								<div style="text-align:center; border-top:1px solid #eee;
								padding:5px 0 0 0;" id="email_footer"> 
								<small style="font-size:11px; color:#999; line-height:14px;">
								</small>
								</div>
					
							</div>
						 
						</body>
						</html>';
						
						//Here put your Validation and send mail
						$sent = wp_mail($donor_useremail, $subject, $body, $headers);						
						
						die(__($message_customer, 'charitable'));
						return;				
					}
						
					//Payment failed
					else
					{
						$message_admin = "iPay88 error description: ".$error_response.".";
					
						self::update_donation_log($donation, $message_admin);
						$donation->update_status('charitable-failed');
								
						$message_customer = "<span style='color:red'>
						<center><h3>Your transaction could not be processed.</h3></span></center>
						<br><br><span style='float:right'><a href=".$gateway->getCancelURL($donation).">Go Back</a></span>";
						
						die(__($message_customer, 'charitable'));
						return;					
					}	
					
				}	
				
				else
				{
					echo "<center><h3>Sorry, you don't have access to open this page!</h3></center>";exit;
					//wp_redirect(get_option('siteurl'));
					//exit;
				}
			}
	
			else
			{
				return;
			}
			
		}
				
    }

endif;

