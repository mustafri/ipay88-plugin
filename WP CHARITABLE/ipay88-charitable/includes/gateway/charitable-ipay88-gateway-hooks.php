<?php 

//Charitable ipay88 Gateway Hooks. 
// author aldamustafri@ipay88.co.id

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Process the donation.  
// @see     Charitable_Gateway_ipay88::process_donation() 

if(-1 == version_compare(charitable()->get_version(), '1.3.0')) 
{
    // This is for backwards-compatibility. Charitable before 1.3 used on ation hook, not a filter.
    // @see    Charitable_Gateway_ipay88::redirect_to_processing_legacy()
    add_action('charitable_process_donation_ipay88', 
	array('Charitable_Gateway_ipay88', 'redirect_to_processing_legacy'));
}

else 
{
    add_filter('charitable_process_donation_ipay88', 
	array('Charitable_Gateway_ipay88', 'redirect_to_processing'), 10, 2);
}

// Render the ipay88 donation processing page content. 
// This is the page that users are redirected to after filling out the donation form. 
// It automatically redirects them to ipay88's website.
// @see Charitable_Gateway_ipay88::process_donation()

add_filter( 'charitable_processing_donation_ipay88', 
array('Charitable_Gateway_ipay88', 'process_donation'), 10, 2);


// Check the response from ipay88 after the donor has completed payment.
// @see Charitable_Gateway_ipay88::ipay88_response()

add_action( 'charitable_donation_receipt_page', 
array('Charitable_Gateway_ipay88', 'ipay88_response'));


// Make the "phone" field required in the donation form since ipay88 requires it.
// @see Charitable_Gateway_ipay88::set_phone_field_required()

add_filter('charitable_donation_form_user_fields',
array( 'Charitable_Gateway_ipay88', 'set_phone_field_required'));


// Change the currency to IDR. 
// @see Charitable_Gateway_ipay88::change_currency_to_idr()
add_action('wp_ajax_charitable_change_currency_to_idr', 
array( 'Charitable_Gateway_ipay88', 'change_currency_to_idr'));


// Change the default gateway to ipay88
// @see Charitable_Gateway_ipay88::change_gateway_to_ipay88()

add_action( 'wp_ajax_charitable_change_gateway_to_ipay88', 
array('Charitable_Gateway_ipay88', 'change_gateway_to_ipay88'));
