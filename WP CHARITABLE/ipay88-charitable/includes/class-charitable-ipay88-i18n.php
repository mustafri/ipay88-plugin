<?php
//Sets up translations for Charitable ipay88.
//author alda.mustafri@ipay88.co.id

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Ensure that Charitable_i18n exists 
if (!class_exists('Charitable_i18n')): 
    return;
endif;


if (!class_exists('Charitable_ipay88_i18n')): 


// Charitable_ipay88_i18n
// @since       1.0.0
class Charitable_ipay88_i18n extends Charitable_i18n 
{
	// @var     string
    protected $textdomain = 'charitable-ipay88';

	
    // Set up the class. 
    // @access  protected
    // @since   1.0.0
	
    protected function __construct() 
	{
        $this->languages_directory = apply_filters('charitable_stripe_languages_directory',
									 'charitable-ipay88/languages' );
        
		$this->locale = apply_filters('plugin_locale', get_locale(), $this->textdomain);
        $this->mofile = sprintf('%1$s-%2$s.mo', $this->textdomain, $this->locale);

        $this->load_textdomain();
    }
    
    public function charitable_start() 
	{
        
    }
}

endif;