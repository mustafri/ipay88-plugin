<?php
/**
 * Plugin Name: iPay88 Payment Gateway for WP Charitable
 * Plugin URI: http://ipay88.co.id/
 * Description: Allows you to use iPay88 Payment Gateway with the WP Charitable plugin.
 * Author: System Engineer iPay88
 * Author URI: http://ipay88.co.id/
 * Version: 1.0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Load plugin class, but only if Charitable is found and activated.
// @return 	void
// @since 	1.0.0

function charitable_ipay88_load() 
{	
	require_once( 'includes/class-charitable-ipay88.php' );

	$has_dependencies = true;

	// Check for Charitable
	if (!class_exists('Charitable')) 
	{
		if (!class_exists('Charitable_Extension_Activation'))
		{
			require_once 'includes/class-charitable-extension-activation.php';
		}

		$activation = new Charitable_Extension_Activation( plugin_dir_path( __FILE__ ), basename( __FILE__ ) );
		$activation = $activation->run();

		$has_dependencies = false;
	} 

	else 
	{
		new Charitable_ipay88( __FILE__ );
	}	
}

add_action( 'plugins_loaded', 'charitable_ipay88_load', 1 );
