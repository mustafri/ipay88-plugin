	<?php

	// iPay88 backend file
	// 2018 iPay88 Indonesia
	// author aldamustafri@ipay88.co.id

	require_once('../../../wp-config.php');
	global $wpdb;		


	if (isset($_REQUEST['TransId']) && isset($_REQUEST['Signature'])) 
	{
		$gateway 				= new Charitable_Gateway_ipay88();
		$prefix  				= $wpdb->prefix;
		$status_pending			= "charitable-pending";
		$status_completed		= "charitable-completed";
		$status_failed			= "charitable-failed";
		
		//Get signature response
		$signature_response		= $_REQUEST['Signature'];
					
		//Generate signature	
		$MechantKey_response 	= $gateway->get_value('merchant_key');			
		$MerchantCode_response 	= $gateway->get_value('merchant_code');
		$PaymentId_response		= $_REQUEST['PaymentId'];
		$RefNo_response 		= trim(stripslashes($_REQUEST['RefNo']));
		$HashAmount_response 	= str_replace(array(',','.'), "", $_REQUEST['Amount']);
		$Currency_response 		= $_REQUEST['Currency'];
		$Status_response 		= $_REQUEST['Status'];
		$error_response         = $_REQUEST['ErrDesc'];
		$transid_response		= $_REQUEST['TransId'];	
				
		$merchant_signature		= "";
			
		$merchant_encrypt		= sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$HashAmount_response.$Currency_response.$Status_response);		
			
		for ($i=0; $i<strlen($merchant_encrypt); $i=$i+2)
		{	
			$merchant_signature .= chr(hexdec(substr($merchant_encrypt,$i,2)));
		}
			
		$merchant_signature_check = base64_encode($merchant_signature);
		
				
		//Payment success and signature match
		if ($_REQUEST['Status']=="1" && $merchant_signature_check==$signature_response) 
		{
			$wpdb->update($prefix.'posts', array('post_status'=>$status_completed), array('ID'=>$RefNo_response));
			print_r("RECEIVEOK");exit;		
		}
			
		//Payment failed
		else
		{
			// $updated = $wpdb->update( $table, $data, $where );
			$wpdb->update($prefix.'posts', array('post_status'=>$status_failed), array('ID'=>$RefNo_response));
			print_r("BACKEND FAILED");exit;
		}			
	}

	
	else
	{
		wp_redirect(get_option('siteurl'));
		exit;
	}
	
	?>