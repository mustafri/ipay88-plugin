<?php

//iPay88 PAYMENT MODULE FOR PRESTASHOP

if (!defined('_PS_VERSION_'))
exit;

class ipay88_kredivo extends PaymentModule
{
	private $_html = '';
	private $_postErrors = array();

	public  $details;
	public  $owner;
	public  $address;
	public  $extra_mail_vars;
		
	public  $ipay88_kredivo_method_title;
	public  $ipay88_kredivo_description;
	public 	$ipay88_kredivo_merchant_code;
	public 	$ipay88_kredivo_merchant_key;
	
	//Options
	public  $ipay88_kredivo_action = "";
		
			
	public function __construct()
	{
		$this->name 			= 'ipay88_kredivo';
		$this->tab 				= 'payments_gateways';
		$this->version 			= '1.7.1';
		$this->author 			= 'iPay88 - System Engineer Officer';
		$this->controllers 		= array('payment', 'validation');
		$this->currencies 		= true;
		$this->currencies_mode 	= 'checkbox';
		
		
		$config = Configuration::getMultiple(array('ipay88_kredivo_method_title','ipay88_kredivo_description','ipay88_kredivo_merchant_code','ipay88_kredivo_merchant_key','ipay88_kredivo_action'));
		
				
		if (isset($config['ipay88_kredivo_method_title']))
			$this->ipay88_kredivo_method_title = $config['ipay88_kredivo_method_title'];
			
		if (isset($config['ipay88_kredivo_description']))
			$this->ipay88_kredivo_description = $config['ipay88_kredivo_description'];
		
		if (isset($config['ipay88_kredivo_merchant_code']))
			$this->ipay88_kredivo_merchant_code = $config['ipay88_kredivo_merchant_code'];	
		
		if (isset($config['ipay88_kredivo_merchant_key']))
			$this->ipay88_kredivo_merchant_key = $config['ipay88_kredivo_merchant_key'];
		
		if (isset($config['ipay88_kredivo_action']))
			$this->ipay88_kredivo_action = $config['ipay88_kredivo_action'];
				
				
		$this->bootstrap = true;
		parent::__construct();
		
				
		$this->displayName 			= $this->l('iPay88 Kredivo Payment');
		$this->description 			= $this->l('Allows you to use iPay88 Kredivo Payment with the Prestashop module.');
		$this->confirmUninstall 	= $this->l('Are you sure about removing these details?');
		
		$this->ps_versions_compliancy 	= array('min' => '1.6.0.1', 'max' => _PS_VERSION_);
				
		if (!isset($this->owner) || !isset($this->details) || !isset($this->address))
			$this->warning = $this->l('Account owner and account details must be configured before using this module.');
		
		if (!count(Currency::checkPaymentCurrencies($this->id)))
			$this->warning = $this->l('No currency has been set for this module.');	
	}

		
	//PARAMETER FOR INSTALL
	public function install()
    {
        if (!parent::install() || !$this->registerHook('payment') || !$this->registerHook('displayPaymentEU') || !$this->registerHook('paymentReturn') || !$this->registerHook('header')) 
		{
            return false;
        }				
						
		$validation_value 	= array (
										'ipay88_kredivo_method_title'	=> '',
										'ipay88_kredivo_description' 	=> '',
										'ipay88_kredivo_merchant_code' 	=> '',
										'ipay88_kredivo_merchant_key' 	=> '',
										'ipay88_kredivo_action' 		=> '',
									);
		
		
		$order 				= array (
										0	=> 'Kredivo Payment Pending',
										1	=> 'Kredivo Payment Success',
										2	=> 'Kredivo Payment Failed',
									);
	
	
		$template_mail 		= array (
										0	=> 'ipay88_kredivo_pending',
										1   => 'ipay88_kredivo_success',
										2	=> 'ipay88_kredivo_failed',	 
									);


		$color 				= array (
										0	=> '#FF8C00',
										1	=> 'LimeGreen',
										2	=> 'Crimson',
									);
					
		
		$config 			= array (
										0		=> 'ipay88_kredivo_pending',
										1		=> 'ipay88_kredivo_success',
										2		=> 'ipay88_kredivo_failed',
									);
		
		
		for($i=0;$i<3;$i++)
		{
			if(!Configuration::get($config[$i]))
			{
				$orderState 			= new OrderState();	
				$orderState->name 		= array();
				$orderState->template	= array();
				
				foreach (Language::getLanguages() AS $language)
				{
					$orderState->name[$language['id_lang']] = $order[$i];
				}
				
				$orderState->send_email = false;
				$orderState->invoice 	= true;
				$orderState->hidden 	= false;
				$orderState->delivery	= false;
				$orderState->logable 	= false;
				$orderState->template	= $template_mail[$i];
				$orderState->color 		= $color[$i];
				
				$orderState->add();
			}
			
			Configuration::updateValue($config[$i], (int)($orderState->id));
		}
		
		
		//added configuration validation value
		Configuration::updateValue('ipay88_kredivo_method_title', 'Kredivo Payment');
		Configuration::updateValue('ipay88_kredivo_description', 'Please note that your Payment is processed by iPay88 Payment Gateway. The page will redirect to iPay88 payment page when you press Confirm Order button.');
		Configuration::updateValue('ipay88_kredivo_merchant_code', $validation_value['ipay88_kredivo_merchant_code']);		
		Configuration::updateValue('ipay88_kredivo_merchant_key', $validation_value['ipay88_kredivo_merchant_key']);
		Configuration::updateValue('ipay88_kredivo_action', $validation_value['ipay88_kredivo_action']);
	
		return true;
													
	}


	//DISPLAY IPAY88 CONFIGIRATION SETTING. CALL PRIVATE METHOD
	public function getContent()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			$this->_postValidation();
			
			if (!count($this->_postErrors))
				$this->_postProcess();
			
			else
				foreach ($this->_postErrors as $err)
				$this->_html .= $this->displayError($err);
		}
		
		else
		
		$this->_html .= '<br />';
		$this->_html .= $this->_displayipay88_kredivo();
		$this->_html .= $this->renderForm();

		return $this->_html;
	}


	//ADMIN -- CONFIGURATION VIEW
	private function _displayipay88_kredivo()
	{
		return $this->display(__FILE__, 'infos.tpl');
	}


	//ADMIN -- CONFIGURATION FORM
	public function renderForm()
	{
		//OPTION						
		$ipay88_kredivo_action = array(
					
			array(
					'id_option' => 'https://sandbox.ipay88.co.id/ePayment/WebService/PaymentAPI/Checkout',
					'name' 		=> 'Sandbox',
				 ),	
								
			array(
					'id_option' => 'https://payment.ipay88.co.id/ePayment/WebService/PaymentAPI/Checkout',
					'name' 		=> 'Production',	
			     ),				
		);
		
			
		//CONFIGURATION FORM
		$fields_form_full = array(
							
			'form'	 => array(
							
			'legend' => array(
								'title' => $this->l('iPay88 Kredivo Payment Configuration'),
								'icon' => 'icon-cogs'
							),
						
			'input' => 	array(	
																				
						array(
						'type'  => 'text',
						'label' => '<span style="color:red"><b>*</b></span> '.$this->l('Method Title'),
						'name'  => 'ipay88_kredivo_method_title',
						'hint'  => array(
									$this->l('This controls the title which the user sees during checkout.')
									),
						 ),
											 
						array(
						'type'  => 'textarea',
						'label' => '<span style="color:red"><b>*</b></span> '.$this->l('Description'),
						'name'  => 'ipay88_kredivo_description',
						'hint'  => array(
									$this->l('This controls the description which the user sees during checkout.')
									),
						),
											 
						array(
						'type'  => 'text',
						'label' => '<span style="color:red"><b>*</b></span> '.$this->l('Merchant Code'),
						'name'  => 'ipay88_kredivo_merchant_code',
						'hint'  => array(
									$this->l('The Merchant Code provided by iPay88 and used to uniquely identify the merchant.'),						
									),
							),
											 
						array(
						'type'  => 'text',
						'label' => '<span style="color:red"><b>*</b></span> '.$this->l('Merchant Key'),
						'name'  => 'ipay88_kredivo_merchant_key',
						'hint'  => array(
								  	$this->l('Provided by iPay88 and shared between iPay88 and merchant only.')
									),
							),
						 
						array(
						'type' 		=> 'select',
						'label' 	=> $this->l('Action to iPay88'),
						'name' 		=> 'ipay88_kredivo_action',
						'required' 	=> false,
						'hint'  	=> array(
										$this->l('Sandbox mode provides you with a chance to test your gateway integration with iPay88. The payment requests will be send to the iPay88 sandbox URL.
Production to start accepting live payment.')
										),
						'options' 	=> array(
												'query' => $ipay88_kredivo_action,
												'id' 	=> 'id_option',
												'name' 	=> 'name'
						 					)
								),
							),
				
					'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
				
		$helper 				= new HelperForm();
		$helper->show_toolbar 	= false;
		
		$helper->table 			= $this->table;
		$lang 					= new Language((int)Configuration::get('PS_LANG_DEFAULT'));

		$helper->default_form_language 		= $lang->id;
				
		$helper->allow_employee_form_lang 	= Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		
		
		$this->fields_form 		= array();
		$helper->id 			= (int)Tools::getValue('id_carrier');
		$helper->identifier 	= $this->identifier;
		$helper->submit_action 	= 'btnSubmit';
		
		
		$helper->currentIndex 	= $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		
		
		$helper->token 		= Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars 	= array(
										'fields_value' 	=> $this->getConfigFieldsValues(),
										'languages' 	=> $this->context->controller->getLanguages(),
										'id_language' 	=> $this->context->language->id
									);

		return $helper->generateForm(array($fields_form_full));

	}
	
	
	//ADMIN -- VALIDATION REQUIRED FORM
	private function _postValidation()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			if (empty($_POST['ipay88_kredivo_method_title']))
				$this->_postErrors[] = $this->l('Method Title is required.');
				
			elseif (empty($_POST['ipay88_kredivo_description']))
				$this->_postErrors[] = $this->l('Description is required');
				
			elseif (empty($_POST['ipay88_kredivo_merchant_code']))
				$this->_postErrors[] = $this->l('Merchant Code is required');
				
			elseif (empty($_POST['ipay88_kredivo_merchant_key']))
				$this->_postErrors[] = $this->l('Merchant Key is required');		
		}
	}
	

	//ADMIN -- GET DATA FROM CONFIGURATION FORM
	public function getConfigFieldsValues()
	{
		return array(
		
		'ipay88_kredivo_method_title'	=> Tools::getValue('ipay88_kredivo_method_title', Configuration::get('ipay88_kredivo_method_title')),	
		
		'ipay88_kredivo_description'	=> Tools::getValue('ipay88_kredivo_description', Configuration::get('ipay88_kredivo_description')),	
		
		'ipay88_kredivo_merchant_code' 	=> Tools::getValue('ipay88_kredivo_merchant_code', Configuration::get('ipay88_kredivo_merchant_code')),	
				
		'ipay88_kredivo_merchant_key' 	=> Tools::getValue('ipay88_kredivo_merchant_key', Configuration::get('ipay88_kredivo_merchant_key')),
		
		'ipay88_kredivo_action' 		=> Tools::getValue('ipay88_kredivo_action', Configuration::get('ipay88_kredivo_action')),	
							
					);
	}
	
	
	//POST FORM CONFIGURATION
	private function _postProcess()
	{
		if (Tools::isSubmit('btnSubmit'))
		{
			//print_r($_POST);exit();
	
			Configuration::updateValue('ipay88_kredivo_method_title',	$_POST['ipay88_kredivo_method_title']);
			Configuration::updateValue('ipay88_kredivo_description',	$_POST['ipay88_kredivo_description']);
			Configuration::updateValue('ipay88_kredivo_merchant_code',	$_POST['ipay88_kredivo_merchant_code']);
			Configuration::updateValue('ipay88_kredivo_merchant_key', 	$_POST['ipay88_kredivo_merchant_key']);
			Configuration::updateValue('ipay88_kredivo_action', 		$_POST['ipay88_kredivo_action']);
					
			$_POST['MY_MODULE_CUSTOMERS'] = implode(',', Tools::getValue('MY_MODULE_CUSTOMERS'));
		}
		
		$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}


	//UNINSTALL
	public function uninstall()
	{	
		if (!Configuration::deleteByName('ipay88_kredivo_method_title')
		|| !Configuration::deleteByName('ipay88_kredivo_description')
	 	|| !Configuration::deleteByName('ipay88_kredivo_merchant_code')
		|| !Configuration::deleteByName('ipay88_kredivo_merchant_key')
		|| !Configuration::deleteByName('ipay88_kredivo_action')			
		|| !parent::uninstall())
			
        return false;
    	
		else
        
		return true;
	}
	
	
	//PASSING PARAMETER TO CUSTOMER CHECKOUT PAYMENT
	public function hookPayment($params)
    {
        if (!$this->active) 
		{
       		return;
        }
        
		if (!$this->checkCurrency($params['cart'])) 
		{
            return;
        }
				
		$view_ipay88_kredivo_method_title	= Configuration::get('ipay88_kredivo_method_title');
        $view_ipay88_kredivo_description    = Configuration::get('ipay88_kredivo_description');
		$view_ipay88_kredivo_merchant_code  = Configuration::get('ipay88_kredivo_merchant_code');
		$view_ipay88_kredivo_merchant_key   = Configuration::get('ipay88_kredivo_merchant_key'); 
		$view_ipay88_kredivo_action    		= Configuration::get('ipay88_kredivo_action');
		
				
		$this->smarty->assign(array(
            'this_path'			=> $this->_path,
            'this_path_bw' 		=> $this->_path,
            'this_path_ssl' 	=> Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/',
			
			'ipay88_kredivo_method_title' 	=> $view_ipay88_kredivo_method_title,
			'ipay88_kredivo_description' 	=> $view_ipay88_kredivo_description,
			'ipay88_kredivo_merchant_code' 	=> $view_ipay88_kredivo_merchant_code,
			'ipay88_kredivo_merchant_key'   => $view_ipay88_kredivo_merchant_key,
			'ipay88_kredivo_action'         => $view_ipay88_kredivo_action,
				
		));
		
        return $this->display(__FILE__, 'payment.tpl');

    }


    public function hookDisplayPaymentEU($params)
    {
        if (!$this->active) 
		{
            return;
        }

        if (!$this->checkCurrency($params['cart'])) 
		{
            return;
        }

        $payment_options = array(
            'cta_text' 	=> $this->l('Pay with iPay88 Kredivo Payment'),
            'logo' 		=> Media::getMediaPath(dirname(__FILE__).'/logo.jpg'),
            'action' 	=> $this->context->link->getModuleLink($this->name, 'validation', array(), true)
        );

        return $payment_options;
		
    }
	
	
	//AFTER REVIEW || STEP 6
	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return;

		$state = $params['objOrder']->getCurrentState();
		if ($state == Configuration::get('ipay88_kredivo_pending') || $state == Configuration::get('PS_OS_PAYMENT'))
		{
			$this->smarty->assign(array(
				'status' 			=> '1',
				'id_order' 			=> $params['objOrder']->id
			));
			
			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
				$this->smarty->assign('reference', $params['objOrder']->reference);
		}
				
		else if ($state == Configuration::get('PS_OS_ERROR')) 
		{
        	$this->smarty->assign(array(
            	'status' => '0',
                'id_order' => $params['objOrder']->id
            ));
			
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
                $this->smarty->assign('reference', $params['objOrder']->reference);
        }
        
		else
		{
			$this->smarty->assign('status', 'other');
		}
			
		return $this->display(__FILE__, 'payment_return.tpl');
	}	
	
	
	//CURRENCY
	public function checkCurrency($cart)
	{
		$currency_order 	= new Currency($cart->id_currency);
		$currencies_module 	= $this->getCurrency($cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
			
			if ($currency_order->id == $currency_module['id_currency'])
				 return true;
		
		return false;
	}
	
		
	//CSS
	public function hookHeader()
    {
		$this->context->controller->addCSS($this->_path.'assets/css/ipay88.css', 'all');
    }
	
}
