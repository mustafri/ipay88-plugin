$(document).ready(function()
{	
	hideOptions('kredivo-full', "class");
	hideOptions('kredivo-installment', "class");
	
	
	
	$("#kredivo_full_btn_on").change(function(e, data)
	{	
		showOptions('kredivo-full', "class");
    });
	
	$("#kredivo_full_btn_off").change(function(e, data)
	{	
    	hideOptions('kredivo-full', "class");
    });
	
	
	$("#kredivo_installment_btn_on").change(function(e, data)
	{	
		showOptions('kredivo-installment', "class");
    });
	
	$("#kredivo_installment_btn_off").change(function(e, data)
	{	
    	hideOptions('kredivo-installment', "class");
    });
	 
	
	function hideOptions(nameClass, type)
	{
    	if(type=="class")
		{
      		$("."+nameClass).closest('.form-group').hide();    
      		$("."+nameClass).closest('.margin-form').hide(); 
      		$("."+nameClass).closest('.margin-form').prev().hide();    
    	} 
		
		else 
		{
		  $("#"+nameClass).closest('.form-group').hide();    
		  $("#"+nameClass).closest('.margin-form').hide();
		  $("#"+nameClass).closest('.margin-form').prev().hide();    
    	}
  	}

  	function showOptions(nameClass, type)
	{
    	if (type=="class")
		{
      		$("."+nameClass).closest('.form-group').show();
      		$("."+nameClass).closest('.margin-form').show();
      		$("."+nameClass).closest('.margin-form').prev().show();
    	}
		
		else
		{
      		$("#"+nameClass).closest('.form-group').show();
      		$("#"+nameClass).closest('.margin-form').show();
      		$("#"+nameClass).closest('.margin-form').prev().show();
    	}	  
  	} 
	
});