<?php
include('../../../../config/config.inc.php');
include('../../../../header.php');
												
	if (isset($_REQUEST['TransId'])) 
	{
		//Get signature response
		$signature_response		= $_REQUEST['Signature'];
			
		//Generate signature	
		$MechantKey_response 	= Configuration::get('ipay88_kredivo_merchant_key');
		$MerchantCode_response 	= Configuration::get('ipay88_kredivo_merchant_code');
		$PaymentId_response		= $_REQUEST['PaymentId'];
		$RefNo_response 		= trim(stripslashes($_REQUEST['RefNo']));
		$HashAmount_response 	= str_replace(array(',','.'), "", $_REQUEST['Amount']);
		$Currency_response 		= $_REQUEST['Currency'];
		$Status_response 		= $_REQUEST['Status'];
		$error_response         = $_REQUEST['ErrDesc'];
		$transid_response		= $_REQUEST['TransId'];	
			
		$merchant_signature		= "";
		
		$merchant_encrypt		= sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$HashAmount_response.$Currency_response.$Status_response);		
		
		for ($i=0; $i<strlen($merchant_encrypt); $i=$i+2)
		{	
			$merchant_signature .= chr(hexdec(substr($merchant_encrypt,$i,2)));
		}
     	
		$merchant_signature_check = base64_encode($merchant_signature);
			
		//Payment success and signature match
		if ($_REQUEST['Status']=="1" && $merchant_signature_check==$signature_response) 
		{
			$order  		= new Order($RefNo_response);
			$ipay88_status 	= Configuration::get('ipay88_kredivo_success');
				
			// Create new OrderHistory
			$history				= new OrderHistory();
			$history->id_order 		= $order->id;
			$history->id_employee 	= 0;
						
			$use_existings_payment = false;
			
			if (!$order->hasInvoice()) $use_existings_payment = true;
			$history->changeIdOrderState($ipay88_status, $order, $use_existings_payment);

			$carrier 		= new Carrier($order->id_carrier, $order->id_lang);
			$templateVars 	= array();

			
			if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
			$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));


			// Save all changes
			if ($history->addWithemail(true, $templateVars))
			{
				//updates stock in shops
				if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
				{
					foreach ($order->getProducts() as $product)
					{
						// If the available quantities depends on the physical stock
						if (StockAvailable::dependsOnStock($product['product_id']))
						{
							//Synchronizes
							StockAvailable::synchronize($product['product_id'], (int)$product['id_shop']);
						}
					}
				}
			}
					
					
			//Update private message
			$query = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Kredivo Payment Completed. Transaction ID: ".$transid_response.".' where id_order = '".$RefNo_response."'";
			
			Db::getInstance()->Execute($query);
			
			//header("Location: ipay88_backend_success");	
			
			$redirect_ipay88_kredivo_backend = Tools::getShopDomainSsl(true).__PS_BASE_URI__.'modules/ipay88_kredivo/controllers/front/ipay88_backend_success.php';
						
			header('Location: '.$redirect_ipay88_kredivo_backend);
			die();						
		}
		
		//Payment failed
		else
		{
			$order = new Order($RefNo_response);
			$ipay88_status = Configuration::get('ipay88_kredivo_failed'); 
			
			// Create new OrderHistory
			$history				= new OrderHistory();
			$history->id_order 		= $order->id;
			$history->id_employee 	= 0;
						
			$use_existings_payment 	= false;
			
			if (!$order->hasInvoice()) $use_existings_payment = true;
			
			//Update order status
			$history->changeIdOrderState($ipay88_status, $order, $use_existings_payment);

			$carrier 		= new Carrier($order->id_carrier, $order->id_lang);
			$templateVars 	= array();

			
			if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
			$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
			
			// Save all changes on order details page 
			if ($history->addWithemail(true, $templateVars))
			{
			}			
		
				
			//Get products by order id
			$products = $order->getProducts();
			
			//Get product attributes value as array
			foreach ($products as $order_product_attributes) 
			{
				$current_quantity[]			= $order_product_attributes['current_stock'];
				$order_quantity[]			= $order_product_attributes['product_quantity'];
				
				$id_product[]				= $order_product_attributes['product_id'];
				$id_product_attributes[]	= $order_product_attributes['product_attribute_id'];	
			}
			
			//Sum two arrays (Current + Order Quantity)
			$available_quantity_attributes =	array_map(function () 
												{
    												return array_sum(func_get_args());
												}, $current_quantity, $order_quantity);
			
			
			//Get product quantity as array
			foreach ($products as $order_product)
			{
				$product_quantitiy[] = Product::getQuantity($order_product['id_product']);
			}
			
			//Sum two arrays (Current + Order Quantity)
			$available_quantity =	array_map(function () 
												{
    												return array_sum(func_get_args());
												}, $product_quantitiy, $order_quantity);
				
				
			//Count loop  						
			$count = count($products);				
			
			//Update quantity attributes
			for($i = 0; $i < $count; $i++)
			{
				$stock = $current_quantity[$i]+$order_quantity[$i];
				
				$restock_parent = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity[$i]." where id_product = ".$id_product[$i]." and id_product_attribute=0";
					
				Db::getInstance()->Execute($restock_parent);
													
				$restock_child = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity_attributes[$i]." where id_product = ".$id_product[$i]." and id_product_attribute = ".$id_product_attributes[$i]." ";
																
				Db::getInstance()->Execute($restock_child);
			}					
								
			//Product Desc       
			foreach ($products as $product_desc) 
			{
				$data['prod_desc'][] = " ".$product_desc['product_name']." +".$product_desc['product_quantity'].", ";
            }	

			$product_description = rtrim(implode($data['prod_desc']), ", ");					

			//Update private message
			$update_message = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Kredivo Payment Failed. Error description: backend signature is not match. Your signature: ".$merchant_signature_check." and iPay88 signature: ".$signature_response." for Transaction ID: ".$transid_response.". Quantity increased for ".$product_description.".' where id_order = '".$RefNo_response."'";
					
			Db::getInstance()->Execute($update_message);
																	
			//print_r("Backend signature is not match. Merchant signature: ".$merchant_signature_check." and iPay88 signature: ".$signature_response." for Transaction ID: ".$transid_response.". and id_order = ".$RefNo_response."");exit;
			
			//header("Location: ipay88_backend_failed");		

			$redirect_ipay88_kredivo_backend = Tools::getShopDomainSsl(true).__PS_BASE_URI__.'modules/ipay88_kredivo/controllers/front/ipay88_backend_failed.php';
						
			header('Location: '.$redirect_ipay88_kredivo_backend);
			die();		
		}			
	}

	else
	{
		Tools::redirectLink(__PS_BASE_URI__.'index.php?controller=history');
	}
?>
