<?php
echo '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>';
 
class ipay88_kredivoValidationModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
		$cart = $this->context->cart;

		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
		Tools::redirect('index.php?controller=order&step=1');

		//Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
		
		$authorized = false;
		
		foreach (Module::getPaymentModules() as $module)
		
		if ($module['name'] == 'ipay88_kredivo')
		{
			$authorized = true;
			break;
		}
			
		if (!$authorized)
			
		die($this->module->l('This payment method is not available.', 'validation'));

		$customer = new Customer($cart->id_customer);
	
		if (!Validate::isLoadedObject($customer))
		Tools::redirect('index.php?controller=order&step=1');

		$ipay88_kredivo 	= new ipay88_kredivo();
		$db 				= Db::getInstance();	
		$currency 			= new Currency((int)$cart->id_currency);	
		
		//Product Desc
		//-1- $products = $order->getProducts();
        $products = $this->context->cart->getProducts(true);
		
		foreach ($products as $product) 
		{
			//-1- $data['prod_desc'][] = " ".$product['product_name']." x ".$product['product_quantity'].", ";
            $data['prod_desc'][] = "".$product['name']." x ".$product['cart_quantity'].", ";
        }	

		$product_description = rtrim(implode($data['prod_desc']), ", ");
		
		//Create order list + update status payment pending
		$ipay88_kredivo->validateOrder($cart->id, Configuration::get('ipay88_kredivo_pending'), $cart->getOrderTotal(true, Cart::BOTH), $ipay88_kredivo->displayName, "iPay88 Payment Pending. Connecting with iPay88 JSON. Quantity reduced for ".$product_description.".", NULL, (int)$currency->id, FALSE, $customer->secure_key);	
		
		$order = new Order($ipay88_kredivo->currentOrder);	
		
		//Payment ID
		$payment_id = '55';
		
		//Currency
		$currency_iso_code 	= $currency->iso_code;
		
		//Amount
		$total 			= $cart->getOrderTotal(true, Cart::BOTH);
		$orderAmount	= number_format($total, 2, ".", "");
	    $HashAmount 	= str_replace(".","",str_replace(",","",$orderAmount));
		
		//Full Transaction Amount
		$FullTransactionAmount 		= $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);	
		$orderFullTransactionAmount	= number_format($FullTransactionAmount, 2, ".", "");
	    $HashFullTransactionAmount 	= str_replace(".","",str_replace(",","",$orderFullTransactionAmount));	
		
		//MISCFEE
		$MiscFee = $HashAmount - $HashFullTransactionAmount;		
					
		//User Contact
		$addressbilling = new Address(intval($cart->id_address_invoice));
		$user_contact 	= $addressbilling->phone;
		
		//Date
		date_default_timezone_set('Asia/Jakarta');	
		
		//Generate Request Signature
		$MechantKey 		= Configuration::get('ipay88_kredivo_merchant_key');
		$MerchantCode 		= Configuration::get('ipay88_kredivo_merchant_code');
		$RefNo				= $order->id;
		$Amount				= $HashAmount; 
		$Currency			= $currency_iso_code;
		$request_signature 	= "";
		
		
		$str = sha1($MechantKey.$MerchantCode.$RefNo.$Amount.$Currency);		
			
		for ($i=0; $i<strlen($str); $i=$i+2)
		{
			$request_signature .= chr(hexdec(substr($str,$i,2)));
		}
		 
		$request_signature = base64_encode($request_signature);
							
		//Response URL
		$responseurl = 
		Tools::getShopDomainSsl(true).__PS_BASE_URI__.'modules/ipay88_kredivo/controllers/front/ipay88_response.php';
		
		//Backend URL
		$backendurl = 
		Tools::getShopDomainSsl(true).__PS_BASE_URI__.'modules/ipay88_kredivo/controllers/front/ipay88_backend.php';
				

		//ITEM TRANSACTION-------------------------------------------------------------------------------
		
		$orderAmount	= number_format($total, 2, ".", "");
	    $HashAmount 	= str_replace(".","",str_replace(",","",$orderAmount));
		
		foreach ($products as $product) 
		{
			$list['id'][] 		= $product['id_product'];
            $list['name'][] 	= $product['name'];
			$list['quantity'][] = $product['cart_quantity'];
			$list['amount'][] 	= str_replace(".","",str_replace(",","",(number_format(($product['price']*$product['cart_quantity']), 2, ".", ""))));
			$list['type'][] 	= $product['category'];	
			$list['url'][] 		= Tools::getShopDomainSsl(true).__PS_BASE_URI__.'index.php?controller=product&id_product='.$product['id_product'];	
        }
						
		$item_by_id 		= $list['id'];
		$item_by_name		= $list['name'];
		$item_by_quantity	= $list['quantity'];
		$item_by_amount		= $list['amount'];
		$item_by_type		= $list['type'];
		$item_by_url		= $list['url'];
			
								
		$item_list = array_map(function () 
					 {
    					return (func_get_args());
					 }, $item_by_id, $item_by_name, $item_by_quantity, $item_by_amount, $item_by_type, $item_by_url);
			
			
		foreach ($item_list as $key => $value )
		{
			$item_list[$key] ['id'] 		= $item_list[$key] ['0'];
			$item_list[$key] ['name'] 		= $item_list[$key] ['1'];
			$item_list[$key] ['quantity'] 	= $item_list[$key] ['2'];
			$item_list[$key] ['amount'] 	= $item_list[$key] ['3'];
			$item_list[$key] ['type'] 		= $item_list[$key] ['4'];
			$item_list[$key] ['url'] 		= $item_list[$key] ['5'];
				
			unset($item_list[$key]['0']);
			unset($item_list[$key]['1']);
			unset($item_list[$key]['2']);
			unset($item_list[$key]['3']);
			unset($item_list[$key]['4']);
			unset($item_list[$key]['5']);
		}
				
					
		//Tax Fee
		$taxfee					= $cart->getOrderTotal(true, Cart::BOTH) - $cart->getOrderTotal(false, Cart::BOTH);
		$taxfeeAmount			= number_format($taxfee, 2, ".", "");
		$taxfee_HashAmount		= str_replace(".","",str_replace(",","",$taxfeeAmount));
		
		//Shipping
		$shippingfee 			= $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);
		$shippingAmount			= number_format($shippingfee, 2, ".", "");
		$shipping_HashAmount	= str_replace(".","",str_replace(",","",$shippingAmount));
		
		//Voucher
		$discount 				= $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS);
		$discountAmount			= number_format($discount, 2, ".", "");
		$discount_HashAmount	= str_replace(".","",str_replace(",","",$discountAmount));	
		
		
		$array_one 				=	array(
											"0"		=> "taxfee",
											"1"		=> "shippingfee",
											"2"		=> "discount",
										);
							
		$array_two				=	array(
											"0"		=> "Tax Fee",
											"1"		=> "Shipping Fee",
											"2"		=> "Discount",
										);	
							
		$array_three			=	array(
											"0"	=> $taxfee_HashAmount,
											"1"	=> $shipping_HashAmount,
											"2"	=> $discount_HashAmount
										);
										
		$array_four				=	array(
											"0"	=> "1",
											"1"	=> "1",
											"2"	=> "1",
										);		
								

		//Make an array that save value based on key from array before					
		$amount_list 	= 	array_map(function () 
							{
    							return (func_get_args());
							}, $array_one, $array_two, $array_three, $array_four);
		
		
		//Rename key of array	
		foreach ($amount_list as $key => $value )
		{
			$amount_list[$key] ['id'] 		= $amount_list[$key] ['0'];
			$amount_list[$key] ['name'] 	= $amount_list[$key] ['1'];
			$amount_list[$key] ['amount']	= $amount_list[$key] ['2'];
			$amount_list[$key] ['quantity'] = $amount_list[$key] ['3'];
				
			unset($amount_list[$key]['0']);
			unset($amount_list[$key]['1']);
			unset($amount_list[$key]['2']);
			unset($amount_list[$key]['3']);
		}
		
		//Add array 
		$amount_list[1]["parent_type"] 	= "SELLER";
		$amount_list[1]["parent_id"] 	= "SELLER456";
		
		//Merge into one array
		$itemTransactions = array_merge($item_list, $amount_list);		
			
		//SHIPPING AND BILLING ADDRESS--------------------------------------------------------------------
		
		$address_shipping 	= new Address(intval($cart->id_address_delivery));
		$address_billing 	= new Address(intval($cart->id_address_invoice));
		
		$ShippingAddress = array(
								'FirstName'		=> $address_shipping->firstname,
								'LastName'		=> $address_shipping->lastname,
								'Address'		=> $address_shipping->address1." ".$address_shipping->address2,
								'City'			=> $address_shipping->city,
								'PostalCode'	=> $address_shipping->postcode,
								'Phone'			=> $address_shipping->phone_mobile,
								'CountryCode'	=> Country::getIsoById($address_shipping->id_country),
							  );
		
		$BillingAddress = array(
								'FirstName'		=> $address_billing->firstname,
								'LastName'		=> $address_billing->lastname,
								'Address'		=> $address_billing->address1." ".$address_billing->address2,
								'City'			=> $address_billing->city,
								'PostalCode'	=> $address_billing->postcode,
								'Phone'			=> $address_billing->phone_mobile,
								'CountryCode'	=> Country::getIsoById($address_billing->id_country),
							  );
		
		
		//GENERATE JSON-------------------------------------------------------------------------------------
			
		$ipay88_Obj = new stdClass();
	
		$ipay88_Obj->MerchantCode 				= $MerchantCode;
		$ipay88_Obj->PaymentId 					= $payment_id;
		$ipay88_Obj->Currency 					= $currency_iso_code;
		$ipay88_Obj->RefNo						= "".$order->id."";
		$ipay88_Obj->Amount						= $HashAmount;
		$ipay88_Obj->ProdDesc					= $product_description;
		$ipay88_Obj->UserName					= $customer->firstname.' '.$customer->lastname;
		$ipay88_Obj->UserEmail					= $customer->email;
		$ipay88_Obj->UserContact				= $user_contact;
		$ipay88_Obj->Remark						= "Transaction date ".date('Y-m-d H:i:s');
		$ipay88_Obj->Lang						= "UTF-8";
		$ipay88_Obj->ResponseURL				= $responseurl;
		$ipay88_Obj->BackendURL					= $backendurl;
		$ipay88_Obj->Signature					= $request_signature;
		$ipay88_Obj->xfield1					= "";
		//$ipay88_Obj->FullTransactionAmount	= $HashFullTransactionAmount;
		//$ipay88_Obj->MiscFee					= $MiscFee;
		
		$ipay88_Obj->itemTransactions			= $itemTransactions;				  
		$ipay88_Obj->ShippingAddress			= $ShippingAddress;
		$ipay88_Obj->BillingAddress				= $BillingAddress;
	
		$ipay88_JSON = json_encode($ipay88_Obj, JSON_UNESCAPED_SLASHES);
		$url  		 = Configuration::get('ipay88_kredivo_action');			
		$options 	 = array(
								'http' 	=> array(
													'header'  	=> "Content-type: application/json\r\n",
													'method'  	=> 'POST',
													'content' 	=> $ipay88_JSON,
												)
							);
											
		$context		= stream_context_create($options);
		$result 		= file_get_contents($url, false, $context);
		$RefNo_response	= $order->id;
		
		//JSON FAILED	
		if ($result === FALSE) 
		{	
			$order			= new Order($RefNo_response);
			$ipay88_status 	= Configuration::get('ipay88_kredivo_failed'); 
			
			// Create new OrderHistory
			$history				= new OrderHistory();
			$history->id_order 		= $order->id;
			$history->id_employee 	= 0;
						
			$use_existings_payment 	= false;
			
			if (!$order->hasInvoice()) $use_existings_payment = true;
			
			//Update order status
			$history->changeIdOrderState($ipay88_status, $order, $use_existings_payment);

			$carrier 		= new Carrier($order->id_carrier, $order->id_lang);
			$templateVars 	= array();

			
			if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
			$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
			
			// Save all changes on order details page 
			if ($history->addWithemail(true, $templateVars))
			{
			}			
		
				
			//Get products by order id
			$products = $order->getProducts();
			
			//Get product attributes value as array
			foreach ($products as $order_product_attributes) 
			{
				$current_quantity[]			= $order_product_attributes['current_stock'];
				$order_quantity[]			= $order_product_attributes['product_quantity'];
				
				$id_product[]				= $order_product_attributes['product_id'];
				$id_product_attributes[]	= $order_product_attributes['product_attribute_id'];	
			}
			
			//Sum two arrays (Current + Order Quantity)
			$available_quantity_attributes =	array_map(function () 
												{
    												return array_sum(func_get_args());
												}, $current_quantity, $order_quantity);
			
			
			//Get product quantity as array
			foreach ($products as $order_product)
			{
				$product_quantitiy[] = Product::getQuantity($order_product['id_product']);
			}
			
			//Sum two arrays (Current + Order Quantity)
			$available_quantity =	array_map(function () 
												{
    												return array_sum(func_get_args());
												}, $product_quantitiy, $order_quantity);
				
				
			//Count loop  						
			$count = count($products);				
			
			//Update quantity attributes
			for($i = 0; $i < $count; $i++)
			{
				$stock = $current_quantity[$i]+$order_quantity[$i];
				
				$update_quantity_attributes = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity_attributes[$i]." where id_product_attribute = ".$id_product_attributes[$i]." ";
				
				Db::getInstance()->Execute($update_quantity_attributes);
				
				$update_quantity = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity[$i]." where id_product = ".$id_product[$i]." and id_product_attribute=0";
				
				Db::getInstance()->Execute($update_quantity);
			}					
								
			//Product Desc       
			foreach ($products as $product_desc) 
			{
				$data['prod_desc'][] = " ".$product_desc['product_name']." +".$product_desc['product_quantity'].", ";
            }	

			$product_description = rtrim(implode($data['prod_desc']), ", ");					

			//Update private message
			$update_message = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Payment Failed. Error description: Connection failed with iPay88 JSON. Quantity increased for ".$product_description.".' where id_order = '".$RefNo_response."'";
			
			Db::getInstance()->Execute($update_message);
																	
			Tools::redirectLink(__PS_BASE_URI__.'index.php?controller=history');					
		}
		
		else
		{
			$data = json_decode($result, true);
			
			//Status Fail
			if ($data['Status'] == "0")
			{	
				$error_response = $data['ErrDesc'];
				$order			= new Order($RefNo_response);

				$ipay88_status 	= Configuration::get('ipay88_kredivo_failed'); 
			
				// Create new OrderHistory
				$history				= new OrderHistory();
				$history->id_order 		= $order->id;
				$history->id_employee 	= 0;
							
				$use_existings_payment 	= false;
			
				if (!$order->hasInvoice()) $use_existings_payment = true;
				
				//Update order status
				$history->changeIdOrderState($ipay88_status, $order, $use_existings_payment);
	
				$carrier 		= new Carrier($order->id_carrier, $order->id_lang);
				$templateVars 	= array();
	
				
				if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
				$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
				
				// Save all changes on order details page 
				if ($history->addWithemail(true, $templateVars))
				{
				}			
			
				
				//Get products by order id
				$products = $order->getProducts();
				
				//Get product attributes value as array
				foreach ($products as $order_product_attributes) 
				{
					$current_quantity[]			= $order_product_attributes['current_stock'];
					$order_quantity[]			= $order_product_attributes['product_quantity'];
					
					$id_product[]				= $order_product_attributes['product_id'];
					$id_product_attributes[]	= $order_product_attributes['product_attribute_id'];	
				}
				
				//Sum two arrays (Current + Order Quantity)
				$available_quantity_attributes =	array_map(function () 
													{
														return array_sum(func_get_args());
													}, $current_quantity, $order_quantity);
				
				
				//Get product quantity as array
				foreach ($products as $order_product)
				{
					$product_quantitiy[] = Product::getQuantity($order_product['id_product']);
				}
				
				//Sum two arrays (Current + Order Quantity)
				$available_quantity =	array_map(function () 
													{
														return array_sum(func_get_args());
													}, $product_quantitiy, $order_quantity);
					
					
				//Count loop  						
				$count = count($products);				
				
				//Update quantity attributes
				for($i = 0; $i < $count; $i++)
				{
					$stock = $current_quantity[$i]+$order_quantity[$i];
					
					$update_quantity_attributes = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity_attributes[$i]." where id_product_attribute = ".$id_product_attributes[$i]." ";
					
					Db::getInstance()->Execute($update_quantity_attributes);
					
					$update_quantity = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity[$i]." where id_product = ".$id_product[$i]." and id_product_attribute=0";
					
					Db::getInstance()->Execute($update_quantity);
				}					
									
				//Product Desc       
				foreach ($products as $product_desc) 
				{
					$data['prod_desc'][] = " ".$product_desc['product_name']." +".$product_desc['product_quantity'].", ";
				}	
	
				$product_description = rtrim(implode($data['prod_desc']), ", ");					
	
				//Update private message
				$update_message = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Payment Failed. Json already connected. Error description: ".$error_response.". Quantity increased for ".$product_description.".' where id_order = '".$RefNo_response."'";
				
				Db::getInstance()->Execute($update_message);
																		
				Tools::redirectLink(__PS_BASE_URI__.'index.php?controller=history');					
			}
		
			//Status Pending
			else
			{
				//Signature from iPay88
				$ipay88_signature  		= $data['Signature'];
			
				//Generate Response Signature				
				$MechantKey_response 	= $MechantKey;
				$MerchantCode_response 	= $MerchantCode;
				$PaymentId_response		= $data['PaymentId'];
				$Amount_response		= $data['Amount'];
				$Currency_response		= $data['Currency'];
				$Status_response		= $data['Status'];
				$merchant_signature 	= "";
			
				$str = sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$Amount_response.$Currency_response.$Status_response);		
				
				for ($i=0; $i<strlen($str); $i=$i+2)
				{
					$merchant_signature .= chr(hexdec(substr($str,$i,2)));
				}
			 
				$merchant_signature = base64_encode($merchant_signature);
			
				//Signature match
				if($merchant_signature == $merchant_signature)
				{
					$order = new Order($RefNo_response);
					
					//Get products by order id
					$products = $order->getProducts();
					
					//Product Desc       
					foreach ($products as $product_desc) 
					{
						$data['prod_desc'][] = " ".$product_desc['product_name']." x".$product_desc['product_quantity'].", ";
					}	
	
					$product_description = rtrim(implode($data['prod_desc']), ", ");
							
					//Update private message
					$update_message = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Payment Pending. Json already connected. Quantity reduced for ".$product_description.".' where id_order = '".$RefNo_response."'";
			
					Db::getInstance()->Execute($update_message);
					
					$checkout	= $data['CheckoutURL'];
					
					$load_file_function = '
					<script language="JavaScript">
						$(document).ready(function() 
						{
							document.ipay88_request_paramaters.submit();
						});
					</script>';
		
					echo $load_file_function;
				
					echo "<body>";
					echo "<form  name='ipay88_request_paramaters' ACTION='".$checkout."' method='POST'>";
					echo "</form>";
				}
			
				//Signature is not match
				else					
				{	
					$order			= new Order($RefNo_response);
					$ipay88_status 	= Configuration::get('ipay88_kredivo_failed'); 
			
					// Create new OrderHistory
					$history				= new OrderHistory();
					$history->id_order 		= $order->id;
					$history->id_employee 	= 0;
								
					$use_existings_payment 	= false;
			
					if (!$order->hasInvoice()) $use_existings_payment = true;
				
					//Update order status
					$history->changeIdOrderState($ipay88_status, $order, $use_existings_payment);
		
					$carrier 		= new Carrier($order->id_carrier, $order->id_lang);
					$templateVars 	= array();
	
				
					if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
					$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
					
					// Save all changes on order details page 
					if ($history->addWithemail(true, $templateVars))
					{
					}			
			
				
					//Get products by order id
					$products = $order->getProducts();
				
					//Get product attributes value as array
					foreach ($products as $order_product_attributes) 
					{
						$current_quantity[]			= $order_product_attributes['current_stock'];
						$order_quantity[]			= $order_product_attributes['product_quantity'];
						
						$id_product[]				= $order_product_attributes['product_id'];
						$id_product_attributes[]	= $order_product_attributes['product_attribute_id'];	
					}
				
					//Sum two arrays (Current + Order Quantity)
					$available_quantity_attributes =	array_map(function () 
														{
															return array_sum(func_get_args());
														}, $current_quantity, $order_quantity);
					
					
					//Get product quantity as array
					foreach ($products as $order_product)
					{
						$product_quantitiy[] = Product::getQuantity($order_product['id_product']);
					}
				
					//Sum two arrays (Current + Order Quantity)
					$available_quantity =	array_map(function () 
														{
															return array_sum(func_get_args());
														}, $product_quantitiy, $order_quantity);
					
					
					//Count loop  						
					$count = count($products);				
					
					//Update quantity attributes
					for($i = 0; $i < $count; $i++)
					{
						$stock = $current_quantity[$i]+$order_quantity[$i];
						
						$update_quantity_attributes = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity_attributes[$i]." where id_product_attribute = ".$id_product_attributes[$i]." ";
						
						Db::getInstance()->Execute($update_quantity_attributes);
						
						$update_quantity = "UPDATE "._DB_PREFIX_."stock_available SET quantity = ".$available_quantity[$i]." where id_product = ".$id_product[$i]." and id_product_attribute=0";
						
						Db::getInstance()->Execute($update_quantity);
					}					
										
					//Product Desc       
					foreach ($products as $product_desc) 
					{
						$data['prod_desc'][] = " ".$product_desc['product_name']." +".$product_desc['product_quantity'].", ";
					}	
	
					$product_description = rtrim(implode($data['prod_desc']), ", ");					
	
					//Update private message
					$update_message = "UPDATE "._DB_PREFIX_."message SET message='iPay88 Payment Failed. Error description: Signature not match. Your signature: ".$merchant_signature." and iPay88 signature: ".$ipay88_signature.". Quantity increased for ".$product_description.".' where id_order = '".$RefNo_response."'";
				
					Db::getInstance()->Execute($update_message);
																		
					Tools::redirectLink(__PS_BASE_URI__.'index.php?controller=history');					
				}
				
			}
		}				
											
		exit();
	}	
}