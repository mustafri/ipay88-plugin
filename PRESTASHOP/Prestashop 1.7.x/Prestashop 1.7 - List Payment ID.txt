====================================================
1. ipay88-prestashop v1.1.2
====================================================
Credit Card (BCA L)			1	
Credit Card (BCA)			41
Credit Card (BCA Authorization)		52
Credit Card (BRI)		  	35
Credit Card (CIMB)		   	42
Credit Card (CIMB Authorization)	56
Credit Card (CIMB IPG)			34
Credit Card (Danamon)			45
Credit Card (Mandiri)			53
Credit Card (Maybank)			43
Credit Card (UnionPay)			54
Credit Card (UOB)			46
		
====================================================
2. ipay88-prestashop v1.2.2
====================================================
Mandiri ClickPay			4
CIMB Clicks				11
Internet Banking Muamalat		14
Danamon Online Banking			23
		
====================================================
3. ipay88-prestashop v1.3.2
====================================================
Maybank VA				9
Mandiri ATM				17
BCA VA					25
BNI VA					26
Permata ATM				31
Permata VA				40

====================================================
4. ipay88-prestashop v1.4.2
====================================================
XL Tunai				7
Mandiri e-cash				13
Tcash					15
Payro					16
Dompetku Plus				18

====================================================
5. ipay88-prestashop v1.5.2
====================================================
Pay by QR 				19

====================================================
6. ipay88-prestashop v1.7.2
====================================================
Kredivo					55
